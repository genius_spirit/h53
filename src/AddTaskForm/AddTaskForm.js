import React from 'react';
import './AddTaskForm.css';


const AddTaskForm = (props) => {
  return(
    <div className="taskForm">
      <h2>To-Do List:</h2>
      <input type='text' placeholder='Add new task' value={props.text} onChange={props.change}/>
      <button onClick={props.click}>Add</button>
    </div>
  )
};

export default AddTaskForm;

