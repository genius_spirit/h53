import React from 'react';
import './Tasks.css';

const Tasks = (props) => {
  return(
    <div className='task-desk'>
      <div className={props.isChecked ? 'task checked' : 'task'}>
        <input type="checkbox" onClick={props.checked}/>
        {props.text}
        <span className='removeBtn' onClick={props.remove}>Remove</span>
      </div>
    </div>
  )
};

export default Tasks;