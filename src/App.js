import React, { Component } from 'react';
import './App.css';
import AddTaskForm from './AddTaskForm/AddTaskForm';
import Tasks from "./Tasks/Tasks";

class App extends Component {

  state = {
    tasks: [

    ],
    currentTask: ''
  };

  componentDidMount = () => {
    const state = localStorage.getItem('state');
    if (!state) {
      const stateToLocalStorage = {
        tasks: [
          {text: 'Buy bread', id: '46464671', checked: false},
          {text: 'clean the room', id: '55843321', checked: false},
          {text: 'do homework', id: '65211452', checked: false}
        ],
        currentTask: ''
      };
      localStorage.setItem('state', JSON.stringify(stateToLocalStorage))
      return this.setState(stateToLocalStorage);
    }
    this.setState(JSON.parse(state))
  };

  currentTask = (event) => {
    const text = event.target.value;
    this.setState({currentTask: text});
  };

  addNewTask = () => {
    if (this.state.currentTask.length > 0) {
      const tasks = [...this.state.tasks];
      const newTask = {text: this.state.currentTask, id: Date.now(), checked: false};
      tasks.push(newTask);
      this.setState({tasks, currentTask: ''});
    } else alert('You have not text in input');

  };

  removeTask = (id) => {
    const index = this.state.tasks.findIndex(p => p.id === id);
    const tasks = [...this.state.tasks];
    tasks.splice(index, 1);
    this.setState({tasks});
  };

  checkTask = (id) => {
    const index = this.state.tasks.findIndex(p => p.id === id);
    const taskStr = {...this.state.tasks[index]};
    taskStr.checked = !taskStr.checked;
    const tasks = [...this.state.tasks];
    tasks[index] = taskStr;
    this.setState({tasks});
  };

  render() {
    return (
      <div className="App">
        <AddTaskForm
          change={(event) => this.currentTask(event)}
          click={() => this.addNewTask()}
          text={this.state.currentTask} />
        {this.state.tasks.map((task) => {
          return <Tasks
            text={task.text}
            key={task.id}
            isChecked={task.checked}
            checked={() => this.checkTask(task.id)}
            remove={() => this.removeTask(task.id)} />
        })}
      </div>
    );
  }
}

export default App;
